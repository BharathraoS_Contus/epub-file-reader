package poc.com.folioreader.utils;

import poc.com.folioreader.folioreader.model.PostData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface APIInterface {
    @GET("/download/{file}")
    void file(@Path("file") String fileId, Callback<PostData> getFile);
}
