package poc.com.folioreader.folioreader.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostData implements Parcelable {

    public static final Creator CREATOR = new Creator() {

        @Override
        public PostData createFromParcel(Parcel source) {
            return new PostData(source);
        }

        @Override
        public PostData[] newArray(int size) {
            return new PostData[size];
        }
    };

    @SerializedName("post_type")
    @Expose
    private String postType;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("user_id")
    @Expose
    private int userId;

    @SerializedName("course_id")
    @Expose
    private int courseId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    @SerializedName("is_active")
    @Expose
    private int isActive;

    @SerializedName("noteable_id")
    @Expose
    private int noteableId;

    @SerializedName("noteable_type")
    @Expose
    private String noteableType;

    @SerializedName("total_comments")
    @Expose
    private int totalComments;

    @SerializedName("total_votes")
    @Expose
    private int totalVotes;

    @SerializedName("total_views")
    @Expose
    private int views;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("reports_count")
    @Expose
    private int reportsCount;

    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("last_name")
    @Expose
    private String lastName;

    @SerializedName("profile_image")
    @Expose
    private String profileImage;

    @SerializedName("role_permission_id")
    @Expose
    private int rolePermissionId;

    @SerializedName("item_image")
    @Expose
    private String itemImage;

    @SerializedName("group")
    @Expose
    private String group;

    @SerializedName("item_type")
    @Expose
    private String itemType;

    @SerializedName("is_upvote")
    @Expose
    private int isVoted;

    @SerializedName("is_favourite")
    @Expose
    private int isFavourite;

    @SerializedName("is_subscribed")
    @Expose
    private int isSubscribed;

    @SerializedName("subscription_type_id")
    @Expose
    private int subscriptionTypeId;

    @SerializedName("book_url")
    @Expose
    private String bookUrl;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("special_price")
    @Expose
    private String specialPrice;

    @SerializedName("special_price_start_date")
    @Expose
    private String specialPriceStartDate;

    @SerializedName("special_price_end_date")
    @Expose
    private String specialPriceEndDate;

    @SerializedName("creator_id")
    @Expose
    private int creatorId;

    @SerializedName("schedule_to_publish")
    @Expose
    private String scheduleToPublish;

    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("refund")
    @Expose
    private String refund;

    @SerializedName("is_ebook_subscribed")
    @Expose
    private int iseBookSubscribed;

    /**
     * Constructor for course model to serialize the arguements.
     *
     * @param in - Parcel object.
     */
    public PostData(Parcel in) {

        this.noteableType = in.readString();

    }

    /**
     * Gets {@see #subscriptionTypeId}
     *
     * @return {@link #subscriptionTypeId}
     */
    public int getSubscriptionTypeId() {
        return subscriptionTypeId;
    }

    /**
     * Sets {@link #subscriptionTypeId}
     */
    public void setSubscriptionTypeId(int subscriptionTypeId) {
        this.subscriptionTypeId = subscriptionTypeId;
    }

    /**
     * Gets {@see #bookUrl}
     *
     * @return {@link #bookUrl}
     */
    public String getBookUrl() {
        return bookUrl;
    }

    /**
     * Sets {@link #bookUrl}
     */
    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }

    /**
     * Gets {@see #price}
     *
     * @return {@link #price}
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets {@link #price}
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets {@see #image}
     *
     * @return {@link #image}
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets {@link #image}
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Gets {@see #specialPrice}
     *
     * @return {@link #specialPrice}
     */
    public String getSpecialPrice() {
        return specialPrice;
    }

    /**
     * Sets {@link #specialPrice}
     */
    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    /**
     * Gets {@see #specialPriceStartDate}
     *
     * @return {@link #specialPriceStartDate}
     */
    public String getSpecialPriceStartDate() {
        return specialPriceStartDate;
    }

    /**
     * Sets {@link #specialPriceStartDate}
     */
    public void setSpecialPriceStartDate(String specialPriceStartDate) {
        this.specialPriceStartDate = specialPriceStartDate;
    }

    /**
     * Gets {@see #specialPriceEndDate}
     *
     * @return {@link #specialPriceEndDate}
     */
    public String getSpecialPriceEndDate() {
        return specialPriceEndDate;
    }

    /**
     * Sets {@link #specialPriceEndDate}
     */
    public void setSpecialPriceEndDate(String specialPriceEndDate) {
        this.specialPriceEndDate = specialPriceEndDate;
    }

    /**
     * Gets {@see #creatorId}
     *
     * @return {@link #creatorId}
     */
    public int getCreatorId() {
        return creatorId;
    }

    /**
     * Sets {@link #creatorId}
     */
    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    /**
     * Gets {@see #scheduleToPublish}
     *
     * @return {@link #scheduleToPublish}
     */
    public String getScheduleToPublish() {
        return scheduleToPublish;
    }

    /**
     * Sets {@link #scheduleToPublish}
     */
    public void setScheduleToPublish(String scheduleToPublish) {
        this.scheduleToPublish = scheduleToPublish;
    }

    /**
     * Gets {@see #updatedAt}
     *
     * @return {@link #updatedAt}
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Sets {@link #updatedAt}
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Gets {@see #refund}
     *
     * @return {@link #refund}
     */
    public String getRefund() {
        return refund;
    }

    /**
     * Sets {@link #refund}
     */
    public void setRefund(String refund) {
        this.refund = refund;
    }

    /**
     * Gets {@see #iseBookSubscribed}
     *
     * @return {@link #iseBookSubscribed}
     */
    public int getIseBookSubscribed() {
        return iseBookSubscribed;
    }

    /**
     * Sets {@link #iseBookSubscribed}
     */
    public void setIseBookSubscribed(int iseBookSubscribed) {
        this.iseBookSubscribed = iseBookSubscribed;
    }

    /**
     * Gets {@see #isSubscribed}
     *
     * @return {@link #isSubscribed}
     */
    public int getIsSubscribed() {
        return isSubscribed;
    }

    /**
     * Sets {@link #isSubscribed}
     */
    public void setIsSubscribed(int isSubscribed) {
        this.isSubscribed = isSubscribed;
    }

    /**
     * @return Gets the value of isVoted and returns isVoted
     */
    public int getIsVoted() {
        return isVoted;
    }

    /**
     * Sets the isVoted You can use getIsVoted() to get the value of isVoted
     */
    public void setIsVoted(int isVoted) {
        this.isVoted = isVoted;
    }

    /**
     * @return Gets the value of isFavourite and returns isFavourite
     */
    public int getIsFavourite() {
        return isFavourite;
    }

    /**
     * Sets the isFavourite You can use getIsFavourite() to get the value of isFavourite
     */
    public void setIsFavourite(int isFavourite) {
        this.isFavourite = isFavourite;
    }

    /**
     * Gets {@see #firstName}
     *
     * @return {@link #firstName}
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets {@link #firstName}
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets {@see #lastName}
     *
     * @return {@link #lastName}
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets {@link #lastName}
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets {@see #profileImage}
     *
     * @return {@link #profileImage}
     */
    public String getProfileImage() {
        return profileImage;
    }

    /**
     * Sets {@link #profileImage}
     */
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * Gets {@see #rolePermissionId}
     *
     * @return {@link #rolePermissionId}
     */
    public int getRolePermissionId() {
        return rolePermissionId;
    }

    /**
     * Sets {@link #rolePermissionId}
     */
    public void setRolePermissionId(int rolePermissionId) {
        this.rolePermissionId = rolePermissionId;
    }

    /**
     * Gets {@see #itemImage}
     *
     * @return {@link #itemImage}
     */
    public String getItemImage() {
        return itemImage;
    }

    /**
     * Sets {@link #itemImage}
     */
    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    /**
     * Gets {@see #group}
     *
     * @return {@link #group}
     */
    public String getGroup() {
        return group;
    }

    /**
     * Sets {@link #group}
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * Gets {@see #itemType}
     *
     * @return {@link #itemType}
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Sets {@link #itemType}
     */
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    /**
     * @return Gets the value of id and returns id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id You can use getId() to get the value of id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return Gets the value of userId and returns userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Sets the userId You can use getUserId() to get the value of userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return Gets the value of courseId and returns courseId
     */
    public int getCourseId() {
        return courseId;
    }

    /**
     * Sets the courseId You can use getCourseId() to get the value of courseId
     */
    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    /**
     * @return Gets the value of title and returns title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title You can use getTitle() to get the value of title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return Gets the value of description and returns description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description You can use getDescription() to get the value of description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Gets the value of imageUrl and returns imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets the imageUrl You can use getImageUrl() to get the value of imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return Gets the value of isActive and returns isActive
     */
    public int getIsActive() {
        return isActive;
    }

    /**
     * Sets the isActive You can use getIsActive() to get the value of isActive
     */
    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    /**
     * @return Gets the value of totalComments and returns totalComments
     */
    /**
     * Gets {@see #noteableId}
     *
     * @return {@link #noteableId}
     */
    public int getNoteableId() {
        return noteableId;
    }

    /**
     * Sets {@link #noteableId}
     */
    public void setNoteableId(int noteableId) {
        this.noteableId = noteableId;
    }

    /**
     * Gets {@see #noteableType}
     *
     * @return {@link #noteableType}
     */
    public String getNoteableType() {
        return noteableType;
    }

    /**
     * Sets {@link #noteableType}
     */
    public void setNoteableType(String noteableType) {
        this.noteableType = noteableType;
    }


    public int getTotalComments() {
        return totalComments;
    }

    /**
     * Sets the totalComments You can use getTotalComments() to get the value of totalComments
     */
    public void setTotalComments(int totalComments) {
        this.totalComments = totalComments;
    }

    /**
     * @return Gets the value of totalVotes and returns totalVotes
     */
    public int getTotalVotes() {
        return totalVotes;
    }

    /**
     * Sets the totalVotes You can use getTotalVotes() to get the value of totalVotes
     */
    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }

    /**
     * @return Gets the value of views and returns views
     */
    public int getViews() {
        return views;
    }

    /**
     * Sets the views You can use getViews() to get the value of views
     */
    public void setViews(int views) {
        this.views = views;
    }

    /**
     * @return Gets the value of createdAt and returns createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets the createdAt You can use getCreatedAt() to get the value of createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return Gets the value of reportsCount and returns reportsCount
     */
    public int getReportsCount() {
        return reportsCount;
    }

    /**
     * Sets the reportsCount You can use getReportsCount() to get the value of reportsCount
     */
    public void setReportsCount(int reportsCount) {
        this.reportsCount = reportsCount;
    }

    /**
     * @return Gets the value of postType and returns postType
     */
    public String getPostType() {
        return postType;
    }

    /**
     * Sets the postType You can use getPostType() to get the value of postType
     */
    public void setPostType(String postType) {
        this.postType = postType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.noteableType);

    }
}
