package poc.com.folioreader.folioreader.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EpubFileDetails {
    @SerializedName("epubfile")
    @Expose
    private PostData ePubFileData;

    /**
     * @return Gets the value of ebookData and returns ebookData
     */
    public PostData getEpubFileData() {
        return ePubFileData;
    }

    /**
     * Sets the ebookData You can use getEbookData() to get the value of ebookData
     */
    public void setEpubFileData(PostData ePubFileData) {
        this.ePubFileData = ePubFileData;
    }

}
