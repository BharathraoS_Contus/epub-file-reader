package poc.com.folioreader.folioreader.view;

public interface EpubFileListener {
    void onItemSelected(String fileName);
}
