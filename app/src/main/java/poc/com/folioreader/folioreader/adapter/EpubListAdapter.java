package poc.com.folioreader.folioreader.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import poc.com.folioreader.databinding.RowLayoutEpubListitemBinding;
import poc.com.folioreader.folioreader.view.EpubFileListener;

public class EpubListAdapter extends RecyclerView.Adapter<EpubListAdapter.EpubListViewHolder> {
    private List<String> epubList;
    private EpubFileListener clickListener;

    public EpubListAdapter(EpubFileListener listener) {
        this.clickListener = listener;
        this.epubList = new ArrayList<>();
    }
    public void setData(List<String> epubList){
        this.epubList = epubList;
    }
    @NonNull
    @Override
    public EpubListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RowLayoutEpubListitemBinding rowLayoutEpubListitemBinding = RowLayoutEpubListitemBinding
                .inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        return new EpubListViewHolder(rowLayoutEpubListitemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final EpubListViewHolder viewHolder, int i) {
        viewHolder.rowLayoutEpubListitemBinding.tvEpubFilename.setText(epubList.get(i));
        viewHolder.rowLayoutEpubListitemBinding.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemSelected(epubList.get(viewHolder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return epubList.size();
    }

    class EpubListViewHolder extends RecyclerView.ViewHolder {
        RowLayoutEpubListitemBinding rowLayoutEpubListitemBinding;

        EpubListViewHolder(@NonNull RowLayoutEpubListitemBinding itemView) {
            super(itemView.getRoot());
            this.rowLayoutEpubListitemBinding = itemView;
        }
    }
}
