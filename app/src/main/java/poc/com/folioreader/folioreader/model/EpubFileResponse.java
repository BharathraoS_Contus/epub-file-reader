package poc.com.folioreader.folioreader.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class EpubFileResponse<T> {
    @SerializedName("error")
    @Nullable
    private boolean error;
    @SerializedName("status")
    @Nullable
    private int statusCode;
    @SerializedName("statusMessage")
    @Nullable
    private String statusMessage;
    @SerializedName("message")
    @Nullable
    private String message;
    @SerializedName("response")
    @Nullable
    private T response;

    @Nullable
    public String getMessage() {
        return message;
    }

    public void setMessage(@Nullable String message) {
        this.message = message;
    }

    @Nullable
    public boolean getError() {
        return error;
    }

    public void setError(@Nullable boolean error) {
        this.error = error;
    }

    @Nullable
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(@Nullable int statusCode) {
        this.statusCode = statusCode;
    }

    @Nullable
    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(@Nullable String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Nullable
    public T getResponse() {
        return response;
    }

    public void setResponse(@Nullable T response) {
        this.response = response;
    }
}
