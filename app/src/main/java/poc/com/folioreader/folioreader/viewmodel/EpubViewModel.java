package poc.com.folioreader.folioreader.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import poc.com.folioreader.databinding.ActivityEpubFileOpenerBinding;
import poc.com.folioreader.folioreader.adapter.EpubListAdapter;
import poc.com.folioreader.folioreader.view.EpubFileListener;

public class EpubViewModel extends ViewModel {
    private ProgressDialog progressCircle = null;
    private ActivityEpubFileOpenerBinding epubFileOpenerBinding;
    private Activity activity;
    private File root;
    ArrayList<String> epubFileList;
    private EpubFileListener listener;
    private EpubListAdapter epubListAdapter;
    private ArrayList<File> fileList = new ArrayList<File>();

    public EpubViewModel(Activity activity, ProgressDialog loading, ActivityEpubFileOpenerBinding epubFileOpenerBinding, EpubFileListener listener) {
        this.progressCircle = loading;
        this.epubFileOpenerBinding = epubFileOpenerBinding;
        this.activity = activity;
        this.listener = listener;
        epubFileList = new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setAdapter();
    }

    private void setAdapter() {
        epubFileOpenerBinding.rvEpubFilelist.setLayoutManager(new LinearLayoutManager(activity));
        epubListAdapter = new EpubListAdapter(listener);
        epubFileOpenerBinding.rvEpubFilelist.setAdapter(epubListAdapter);
    }

    public void onEpubFileBrowserClicked() {
        if (checkPermissionForReadExtertalStorage()) {
            root = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/Download/");
            if (!epubFileList.isEmpty()) {
                epubFileList.clear();
            }
            addList();
            epubListAdapter.notifyDataSetChanged();
            epubListAdapter.setData(epubFileList);
        } else {
            try {
                requestPermissionForReadExtertalStorage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        String path = Environment.getExternalStorageDirectory()+"/Download";
//        Log.d("Files", "Path: " + path);
//        File directory = new File(path);
//        File[] files = directory.listFiles();
//        Log.d("Files", "Size: "+ files.length);
//        for (int i = 0; i < files.length; i++)
//        {
//            Log.d("Files", "FileName:" + files[i].getName());
//        }


//        Intent intent = new Intent();
//        intent.setType("application/epub+zip");
//        Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath());
//        if (Build.VERSION.SDK_INT < 19) {
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            intent = Intent.createChooser(intent, "Select .epub file");
//        } else {
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//        }
//        activity.startActivityForResult(intent, 1);
    }

    public void addList() {
        if(!fileList.isEmpty()){
            fileList.clear();
        }
        getfile(root);
        for (int i = 0; i < fileList.size(); i++) {
            epubFileList.add(fileList.get(i).getName());
        }
        if(epubFileList.isEmpty()){
            epubFileList.add("No Files Found with .epub extension!");
        }
    }

    public boolean checkPermissionForReadExtertalStorage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    public void requestPermissionForReadExtertalStorage() throws Exception {
        try {
            ActivityCompat.requestPermissions((Activity) activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    2);
        } catch (Exception e) {
            e.getMessage();
            throw e;
        }
    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {

//                if (listFile[i].isDirectory()) {
//                    fileList.add(listFile[i]);
//                    getfile(listFile[i]);
//
//                } else {
                if (listFile[i].getName().endsWith(".epub")) {
                    fileList.add(listFile[i]);
                }
//                }

            }
        }
        return fileList;
    }

    private void showProgressDialog(int downloadedSize, int totalSize) {
        int downloadedPercentage;
        downloadedPercentage = (downloadedSize / totalSize) * 100;
        progressCircle.setCancelable(true);
        progressCircle.setMessage("Downloading epub file...");
        progressCircle.setMax(100);
        progressCircle.setProgress(downloadedPercentage);
        progressCircle.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressCircle.show();
    }

    public void dismissProgressDialog() {
        progressCircle.dismiss();
    }

    private void downloadFile() {
        try {
            //set the download URL, a url that points to a file on the internet
            //this is the file to be downloaded
            URL url = new URL("http://pulse35.contus.us/download/varun.epub");

            //create the new connection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            //set up some things on the connection
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            //and connect!
            urlConnection.connect();

            //set the path where we want to save the file
            //in this case, going to save it on the root directory of the
            //sd card.
            File sdCardRoot = Environment.getExternalStorageDirectory();
            //create a new file, specifying the path, and the filename
            //which we want to save the file as.
            File file = new File(sdCardRoot, "epubfile.epub");

            //this will be used to write the downloaded data into the file we created
            try (FileOutputStream fileOutput = new FileOutputStream(file)) {

                //this will be used in reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file
                int totalSize = urlConnection.getContentLength();
                //variable to store total downloaded bytes
                int downloadedSize = 0;

                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0; //used to store a temporary size of the buffer

                //now, read through the input buffer and write the contents to the file
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    //add the data in the buffer to the file in the file output stream (the file on the sd card
                    fileOutput.write(buffer, 0, bufferLength);
                    //add up the size so we know how much is downloaded
                    downloadedSize += bufferLength;
                    //this is where you would do something to report the prgress, like this maybe
                    showProgressDialog(downloadedSize, totalSize);

                }
                //close the output stream when done
            }

            //catch some possible errors...
        } catch (IOException e) {
            e.getMessage();
        }
    }
}
