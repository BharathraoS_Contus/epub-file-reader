package poc.com.folioreader.folioreader.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.folioreader.FolioReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import poc.com.folioreader.R;
import poc.com.folioreader.databinding.ActivityEpubFileOpenerBinding;
import poc.com.folioreader.folioreader.viewmodel.EpubViewModel;

public class EpubFileOpenerActivity extends AppCompatActivity implements EpubFileListener {
    ProgressDialog progressCircle;
    EpubViewModel viewModel;
    FolioReader folioReader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_epub_file_opener);
        progressCircle = new ProgressDialog(this);
        folioReader = FolioReader.get();
        ActivityEpubFileOpenerBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_epub_file_opener);
//        EpubViewModel epubViewModel = ViewModelProviders.of(this).get(EpubViewModel.class);
        viewModel = new EpubViewModel(this, progressCircle, activityMainBinding, this);
        activityMainBinding.setViewModel(viewModel);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1
                && resultCode == Activity.RESULT_OK && data != null) {
            Uri uri = data.getData();
            if (uri != null) {
                String uriString = uri.toString();
                File myFile = new File(uriString);
                try {
                    InputStream my = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                String path = uri.getPath();
                folioReader.openBook(path);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 2) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                viewModel.onEpubFileBrowserClicked();
            else
                Toast.makeText(this,"Permission Denied for Accessing External Storage",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemSelected(String fileName) {
        Log.d("TAG",fileName);
        if(!fileName.equals("No Files Found with .epub extension!"))
        folioReader.openBook(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/Download/"+fileName);
    }
}
